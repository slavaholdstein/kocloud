'use strict';

app.controller('ItemEditCtrl', function ($scope, $location, $routeParams, Item) {

	$scope.item = Item.get({id: $routeParams.id});
	
	$scope.updateItem = function (item) {
		Item.save(item);
      	$location.path('/');
    };

    $scope.cancel = function () {
      $location.path('/');
    };

});