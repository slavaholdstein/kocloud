'use strict';

app.controller('UserCtrl', function ($scope, $location, User) {
    $scope.login = function () {
    	User.login($scope.user)
    	.success(function () {
        	//AuthUser.isLogged = true;
        	$location.path('/');
    	})
    }
});
