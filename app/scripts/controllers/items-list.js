'use strict';

app.controller('ItemsListCtrl', function ($scope, $location, Item) {
  $scope.item = new Item();
  $scope.items = Item.query();

  $scope.newFolder = function () {
    $scope.item.Id = 0, 
    $scope.item.Name = "New folder", 
    $scope.item.FileType = 0,
    $scope.item.UID = "newFolderId"
    
    $scope.editing = true;
  }

  $scope.saveItem = function () {
    $scope.item.$save();

    $scope.editing = false;
    $scope.item = new Item();
    $scope.items = Item.query();
  };

  $scope.editItem = function (item) {
    $location.path('/items/' + item.Id + '/edit');
  }
    
  $scope.deleteItem = function (item) {
  	Item.delete({Id: item.Id}, function () {
      var items = $scope.items;
      for (var itemKey in items) {
        if (items[itemKey].Id == item.Id) {
          $scope.items.splice(itemKey, 1);
          break;
        }
      }
    });
  }

  $scope.cancel = function () {
    $scope.editing = false;
    $location.path('/');
  };
});
