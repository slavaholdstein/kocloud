'use strict';
/* global app:true */
/* exported app */
/**
 * @ngdoc overview
 * @name angNewsApp
 * @description
 * # angNewsApp
 *
 * Main module of the application.
 */

var app = angular
  .module('angNewsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'UserCtrl'
      })
      .when('/', {
        templateUrl: 'views/items-list.html',
        controller: 'ItemsListCtrl'
      })
      .when('/items/:id/edit', {
        templateUrl: 'views/item-edit.html',
        controller: 'ItemEditCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });