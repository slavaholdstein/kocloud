'use strict';

app.factory('User', function($http){
	return {
		login: function (user) {
			return $http.post('http://dev.kocloud.net/api/Auth/Login', { username: user.name, 
																		password: user.password, 
																		rememberMe: user.rememberMe });
		}
	};
});