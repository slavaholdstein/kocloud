'use strict';

app.factory('Item', function ($resource){
	return $resource('http://dev.kocloud.net/api/FSItem/:id', {}, {
		query: { method: 'GET', isArray: true },
		save: { method: 'POST' }
	});
});